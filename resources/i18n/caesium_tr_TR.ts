<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr_TR">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../../views/AboutDialog.ui" line="26"/>
        <source>About Caesium - Image Compressor</source>
        <translation>Caesium - Image Compressor Hakkında</translation>
    </message>
    <message>
        <location filename="../../views/AboutDialog.ui" line="104"/>
        <source>Caesium Image Compressor</source>
        <translation>Caesium Image Compressor</translation>
    </message>
    <message>
        <location filename="../../views/AboutDialog.ui" line="148"/>
        <source>Check for updates</source>
        <translation>Güncellemeleri kontrol et</translation>
    </message>
    <message>
        <location filename="../../views/AboutDialog.ui" line="205"/>
        <source>Copyright © 2022 Matteo Paonessa.
All rights reserved.</source>
        <translation>Copyright © 2022 Matteo Paonessa.
All rights reserved.</translation>
    </message>
</context>
<context>
    <name>CImageTreeModel</name>
    <message>
        <location filename="../../src/models/CImageTreeModel.cpp" line="11"/>
        <source>Name</source>
        <translation>Ad</translation>
    </message>
    <message>
        <location filename="../../src/models/CImageTreeModel.cpp" line="11"/>
        <source>Size</source>
        <translation>Boyut</translation>
    </message>
    <message>
        <location filename="../../src/models/CImageTreeModel.cpp" line="11"/>
        <source>Resolution</source>
        <translation>Çözünürlük</translation>
    </message>
    <message>
        <location filename="../../src/models/CImageTreeModel.cpp" line="11"/>
        <source>Saved</source>
        <translation>Kaydedildi</translation>
    </message>
    <message>
        <location filename="../../src/models/CImageTreeModel.cpp" line="11"/>
        <source>Info</source>
        <translation>Bilgi</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../views/MainWindow.ui" line="17"/>
        <source>Caesium Image Compressor</source>
        <translation>Caesium Image Compressor</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="126"/>
        <source>Add...</source>
        <translation>Ekle...</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="146"/>
        <location filename="../../views/MainWindow.ui" line="1308"/>
        <source>Remove</source>
        <translation>Kaldır</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="179"/>
        <location filename="../../views/MainWindow.ui" line="1385"/>
        <location filename="../../views/MainWindow.ui" line="1388"/>
        <source>Compress</source>
        <translation>Sıkıştır</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="207"/>
        <source>Compression</source>
        <translation>Sıkıştırmak</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="227"/>
        <source>JPEG</source>
        <translation>JPEG</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="258"/>
        <location filename="../../views/MainWindow.ui" line="365"/>
        <location filename="../../views/MainWindow.ui" line="412"/>
        <source>Quality</source>
        <translation>Kalite</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="312"/>
        <source>PNG</source>
        <translation>PNG</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="394"/>
        <source>WebP</source>
        <translation>WebP</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="479"/>
        <source>Lossless</source>
        <translation>Kayıpsız</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="486"/>
        <source>Keep Metadata</source>
        <translation>Meta Verileri Sakla</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="509"/>
        <source>Resize</source>
        <translation>Boyutlandır</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="535"/>
        <source>Resize to fit</source>
        <translation>Sığacak şekilde yeniden boyutlandır</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="559"/>
        <source>No resize</source>
        <translation>Yeniden boyutlandırma</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="564"/>
        <source>Dimensions</source>
        <translation>Boyutlar</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="569"/>
        <source>Percentage</source>
        <translation>Yüzde</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="574"/>
        <source>Short edge</source>
        <translation>Kısa kenar</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="579"/>
        <source>Long edge</source>
        <translation>Uzun kenar</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="615"/>
        <source>Width</source>
        <translation>Genişlik</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="643"/>
        <location filename="../../views/MainWindow.ui" line="687"/>
        <location filename="../../views/MainWindow.ui" line="744"/>
        <location filename="../../src/MainWindow.cpp" line="767"/>
        <location filename="../../src/MainWindow.cpp" line="771"/>
        <source>px</source>
        <translation>px</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="659"/>
        <source>Size</source>
        <translation>Boyut</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="716"/>
        <source>Height</source>
        <translation>Yükseklik</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="762"/>
        <source>Do not enlarge</source>
        <translation>Büyütme</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="772"/>
        <source>Keep aspect ratio</source>
        <translation>En-boy oranını koru</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="799"/>
        <source>Output</source>
        <translation>Çıktı</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="819"/>
        <source>Folder</source>
        <translation>Dosya</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="863"/>
        <source>Select...</source>
        <translation>Seç...</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="875"/>
        <source>Same folder as input</source>
        <translation>Giriş klasörü ile aynı klasör</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="885"/>
        <source>Keep folder structure</source>
        <translation>Klasör yapısını koru</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="892"/>
        <source>Skip if output size is bigger than the original</source>
        <translation>Çıktı boyutu orijinalden büyükse atla</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="909"/>
        <source>Keep file dates</source>
        <translation>Dosya tarihlerini sakla</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="924"/>
        <source>Creation</source>
        <translation>Oluşturma</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="931"/>
        <source>Last modified</source>
        <translation>Son değişiklik</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="938"/>
        <source>Last access</source>
        <translation>Son erişim</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="954"/>
        <source>Suffix</source>
        <translation>Son ek</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="1028"/>
        <location filename="../../src/MainWindow.cpp" line="439"/>
        <location filename="../../src/MainWindow.cpp" line="631"/>
        <source>Cancel</source>
        <translation>İptal Et</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="1108"/>
        <source>Compressed</source>
        <translation>Sıkıştırılmış</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="1115"/>
        <source>Original</source>
        <translation>Orijinal</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="1164"/>
        <source>Help</source>
        <translation>Yardım</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="1172"/>
        <source>File</source>
        <translation>Dosya</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="1181"/>
        <source>Edit</source>
        <translation>Düzenle</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="1192"/>
        <source>View</source>
        <translation>Göster</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="1196"/>
        <location filename="../../views/MainWindow.ui" line="1219"/>
        <source>Toolbar</source>
        <translation>Araç çubuğu</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="1261"/>
        <source>About Caesium Image Compressor</source>
        <translation>Caesium Image Compressor Hakkında</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="1273"/>
        <source>Add files...</source>
        <translation>Dosyaları ekle...</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="1276"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="1285"/>
        <source>Add folder...</source>
        <translation>Klasör ekle...</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="1288"/>
        <source>Ctrl+Shift+O</source>
        <translation>Ctrl+Shift+O</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="1293"/>
        <location filename="../../src/MainWindow.cpp" line="158"/>
        <source>Exit</source>
        <translation>Çıkış</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="1311"/>
        <source>Del</source>
        <translation>Sil</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="1316"/>
        <source>Select All</source>
        <translation>Hepsini Seç</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="1319"/>
        <source>Ctrl+A</source>
        <translation>Ctrl+A</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="1331"/>
        <source>Clear</source>
        <translation>Temizle</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="1342"/>
        <source>Show previews</source>
        <translation>Önizlemeleri göster</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="1351"/>
        <source>Preferences...</source>
        <translation>Tercihler...</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="1359"/>
        <source>Show original in file manager</source>
        <translation>Dosya yöneticisinde orijinali göster</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="1364"/>
        <source>Show compressed in file manager</source>
        <translation>Dosya yöneticisinde sıkıştırılmışı göster</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="1373"/>
        <source>Donate</source>
        <translation>Bağış</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="1399"/>
        <source>Icons</source>
        <translation>Simgeler</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="1407"/>
        <source>Icons and Text</source>
        <translation>Simgeler ve Metin</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="1415"/>
        <source>Hide</source>
        <translation>Gizle</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="1420"/>
        <source>Show</source>
        <translation>Göster</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="1428"/>
        <source>Auto preview</source>
        <translation>Otomatik önizleme</translation>
    </message>
    <message>
        <location filename="../../views/MainWindow.ui" line="1440"/>
        <location filename="../../src/MainWindow.cpp" line="1089"/>
        <source>Preview</source>
        <translation>Ön izleme</translation>
    </message>
    <message>
        <location filename="../../src/MainWindow.cpp" line="218"/>
        <source>Import files...</source>
        <translation>Dosyaları içe aktar...</translation>
    </message>
    <message>
        <location filename="../../src/MainWindow.cpp" line="234"/>
        <source>Import folder...</source>
        <translation>Klasörü içe aktar...</translation>
    </message>
    <message>
        <location filename="../../src/MainWindow.cpp" line="439"/>
        <source>Importing files...</source>
        <translation>Dosyalar içe aktarılıyor...</translation>
    </message>
    <message>
        <location filename="../../src/MainWindow.cpp" line="526"/>
        <source>You are about to overwrite your original images and this action can&apos;t be undone.

Do you really want to continue?</source>
        <translation>Gerçekten devam etmek istiyor musun?</translation>
    </message>
    <message>
        <location filename="../../src/MainWindow.cpp" line="529"/>
        <location filename="../../src/MainWindow.cpp" line="630"/>
        <source>Yes</source>
        <translation>Evet</translation>
    </message>
    <message>
        <location filename="../../src/MainWindow.cpp" line="530"/>
        <source>No</source>
        <translation>Hayır</translation>
    </message>
    <message>
        <location filename="../../src/MainWindow.cpp" line="560"/>
        <location filename="../../src/MainWindow.cpp" line="1170"/>
        <source>Compressing...</source>
        <translation>Sıkıştırılıyor...</translation>
    </message>
    <message>
        <location filename="../../src/MainWindow.cpp" line="627"/>
        <source>Do you really want to quit?</source>
        <translation>Gerçekten çıkmak istiyor musun?</translation>
    </message>
    <message>
        <location filename="../../src/MainWindow.cpp" line="650"/>
        <source>Select output folder...</source>
        <translation>Çıktı klasörünü seç...</translation>
    </message>
    <message>
        <location filename="../../src/MainWindow.cpp" line="716"/>
        <source>Compression finished!</source>
        <translation>Sıkıştırma bitti!</translation>
    </message>
    <message>
        <location filename="../../src/MainWindow.cpp" line="719"/>
        <source>You just saved %1!</source>
        <translation>Az önce kaydedilen %1!</translation>
    </message>
    <message>
        <location filename="../../src/MainWindow.cpp" line="728"/>
        <source>Total files: %1
Original size: %2
Compressed size: %3
Saved: %4 (%5%)</source>
        <translation>Toplam dosya: %1
Orijinal boyut: %2
Sıkıştırılmış boyut: %3
Kaydedildi: %4 (%5%)</translation>
    </message>
    <message>
        <location filename="../../src/MainWindow.cpp" line="781"/>
        <location filename="../../src/MainWindow.cpp" line="785"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../../src/MainWindow.cpp" line="899"/>
        <source>images in list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/MainWindow.cpp" line="1103"/>
        <source>Finishing...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../../views/PreferencesDialog.ui" line="20"/>
        <source>Preferences</source>
        <translation>Tercihler</translation>
    </message>
    <message>
        <location filename="../../views/PreferencesDialog.ui" line="37"/>
        <source>Close</source>
        <translation>Kapat</translation>
    </message>
    <message>
        <location filename="../../views/PreferencesDialog.ui" line="69"/>
        <location filename="../../views/PreferencesDialog.ui" line="140"/>
        <source>General</source>
        <translation>Genel</translation>
    </message>
    <message>
        <location filename="../../views/PreferencesDialog.ui" line="81"/>
        <location filename="../../views/PreferencesDialog.ui" line="384"/>
        <source>Language</source>
        <translation>Dil</translation>
    </message>
    <message>
        <location filename="../../views/PreferencesDialog.ui" line="147"/>
        <source>Send anonymous usage reports</source>
        <translation>Anonim kullanım raporlarını gönder</translation>
    </message>
    <message>
        <location filename="../../views/PreferencesDialog.ui" line="186"/>
        <location filename="../../views/PreferencesDialog.ui" line="323"/>
        <source>Changes will apply after restart</source>
        <translation>Değişiklikler yeniden başlatıldıktan sonra uygulanacak</translation>
    </message>
    <message>
        <location filename="../../views/PreferencesDialog.ui" line="196"/>
        <source>Theme (experimental)</source>
        <translation>Tema (deneysel)</translation>
    </message>
    <message>
        <location filename="../../views/PreferencesDialog.ui" line="244"/>
        <source>Advanced</source>
        <translation>Gelişmiş</translation>
    </message>
    <message>
        <location filename="../../views/PreferencesDialog.ui" line="251"/>
        <source>Multithreading</source>
        <translation>Multithreading</translation>
    </message>
    <message>
        <location filename="../../views/PreferencesDialog.ui" line="277"/>
        <source>Check updates at startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../views/PreferencesDialog.ui" line="284"/>
        <source>Prompt before exit</source>
        <translation>Çıkmadan önce sor</translation>
    </message>
    <message>
        <location filename="../../views/PreferencesDialog.ui" line="291"/>
        <source>Import files in subfolders when opening a directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../views/PreferencesDialog.ui" line="310"/>
        <source>Preferred language</source>
        <translation>Tercih edilen dil</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/PreferencesDialog.cpp" line="20"/>
        <source>Show usage data</source>
        <translation>Kullanım verisini göster</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/PreferencesDialog.cpp" line="119"/>
        <source>Usage data</source>
        <translation>Kullanım verisi</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/PreferencesDialog.cpp" line="120"/>
        <source>This data is collected to provide the best long term support for the application. No data is sent to third parties.</source>
        <translation>Bu veriler, uygulama için en iyi uzun vadeli desteği sağlamak için toplanır. Üçüncü taraflara hiçbir veri gönderilmez.</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/PreferencesDialog.cpp" line="121"/>
        <source>System data</source>
        <translation>Sistem verisi</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/PreferencesDialog.cpp" line="121"/>
        <source>Compression data</source>
        <translation>Sıkıştırma verileri</translation>
    </message>
</context>
<context>
    <name>QIODevice</name>
    <message>
        <location filename="../../src/MainWindow.cpp" line="220"/>
        <source>Image Files</source>
        <translation>Resim Dosyaları</translation>
    </message>
    <message>
        <location filename="../../src/models/CImage.cpp" line="129"/>
        <source>Input file does not exist</source>
        <translation>Giriş dosyası mevcut değil</translation>
    </message>
    <message>
        <location filename="../../src/models/CImage.cpp" line="148"/>
        <source>Cannot make output path, check your permissions</source>
        <translation>Çıktı yolu oluşturulamadı, izinlerinizi kontrol edin</translation>
    </message>
    <message>
        <location filename="../../src/models/CImage.cpp" line="165"/>
        <source>Temporary file creation failed</source>
        <translation>Geçici dosya oluşturulamadı</translation>
    </message>
    <message>
        <location filename="../../src/models/CImage.cpp" line="196"/>
        <source>Skipped: compressed file is bigger than original</source>
        <translation>Atlandı: sıkıştırılmış dosya orijinalden daha büyük</translation>
    </message>
    <message>
        <location filename="../../src/models/CImage.cpp" line="207"/>
        <source>Cannot copy output file, check your permissions</source>
        <translation>Çıktı dosyası kopyalanamıyor, izinlerinizi kontrol edin</translation>
    </message>
    <message>
        <location filename="../../src/models/CImage.cpp" line="335"/>
        <source>Compressing...</source>
        <translation>Sıkıştırılıyor...</translation>
    </message>
    <message>
        <location filename="../../src/models/CImage.cpp" line="337"/>
        <source>Error:</source>
        <translation>Hata:</translation>
    </message>
    <message>
        <location filename="../../src/utils/Utils.cpp" line="134"/>
        <source>File not found</source>
        <translation>Dosya bulunamadı</translation>
    </message>
</context>
</TS>
